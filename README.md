## TIPE 2023 - Louis-Max Harter

Le but de ce TIPE est de pouvoir retrouver sa position dans l'espace après avoir mesuré les réseaux wifis environnants puis en procédant à différentes interpolations.

Cependant, si un usagé possède suffisament de points d'accès à disposition (centre commercial, entreprise privée), cela permettrait à l'usagé de retrouver la posssition des points environnents.

Ce TIPE est uniquement à but pédagogique.

![Reconstitution d'un réseaux wifi](./exemples/wifi.png)
![Les RBFs utilisés](./exemples/rbf.png)
![Résultats](./exemples/resultats.png)

Une précision de 3.7m à été obtenus dans une zone d'étude de 20 par 25m.