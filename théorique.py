import math
import random
import numpy as np
from copy import deepcopy
from vracs import *
from collections import namedtuple
from scipy.interpolate import Rbf
from matplotlib import pyplot as plt, cm
from matplotlib import style

PGF = False

import matplotlib
if PGF :
    matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })

"""
Spots : {spotsID : samplings, ...}
Samplings : [(x,y,weigth), ...]

scans: {scanID:scan, ...}
scan: namedtuple("Scan", "date", "time", "ID", "desc", "x", "y", "wifis")
wifis: [(spotBSSID, spotSSID, rate, signal), ...]

Spots : {wifiID : samplings, ...}
Samplings : [(x,y,weigth), ...]
Request : {spotsID: weigth, ...}
"""

def scipy_RBF_generator(samplings, f="gaussian", smooth=0):
    x,y,w = rotate(samplings)
    return Rbf(x,y,w, function=f, smooth=smooth)

def scipy_RBF_generator_generator(
        f=["inverse", "gaussian",
           "linear", "cubic",
           "quintic", "thin_plate",
           "multiquadric"
        ]):
    """generate scipy_RBF_generator for each kernel"""
    return [
        lambda sampling: scipy_RBF_generator(sampling, f=kernel)
        for kernel in f
    ]

class Heatmap():
    def __init__(self, spots, RBF):
        # Basic informations
        self.spots = spots
        self.wifis = set(spots.keys())
        self.samplings = list(set([
            (x,y)
            for wifi, samplings_detected in spots.items()
            for x, y, w in samplings_detected
        ]))

        # RBF settings
        self.RBF = RBF
        self.smooth = 0
        self.epsilon = None
        self.fct_mean = mean_quadratic
        self.fct_abs = abs_diff_square

        # Heatmap scope and size
        self.step = 100
        self.x_range, self.y_range = spots_scope(spots)
        self.x_linspace =  np.linspace(*self.x_range, num=self.step)
        self.y_linspace =  np.linspace(*self.y_range, num=self.step)

        # Cache for wifis RBF
        self.wifis_RBF = {}
        self.wifis_heatmap = {}
        #self.generate_wifis_heatmap()

        # Result of request
        self.request = None
        self.goal = None
        self.best = None
        self.heatmap = None

        # Plotting settings
        self.annotate = {}
        self.title = None
        self.note = None

        #self.border_distance = float("inf")
        self.border_distance = 10
        self.border = []
        self.score_outside = -10
        self.set_border()

        self.precision = None

    def set_border(self):
        self.border = [[
                self.distance_nearest_spot(x,y) < self.border_distance
                for x in self.x_linspace
            ]   for y in self.y_linspace
        ]

    def distance_nearest_spot(self, x,y):
        mini = float("inf")
        for XY in self.samplings:
            mini = min(mini, distance((x,y), XY))
        return mini

    def generate_wifis_heatmap(self, extra_zero=True):
        """Generate wifi heatmap"""
        """
        for n, (wifiID, samplings) in progressbar(
                enumerate(self.spots.items()),
                len(self.spots),
                "Generating wifis heatmaps:", 60):
        """
        for n, (wifiID, samplings) in enumerate(self.spots.items()):
            if extra_zero:
                detected = set((x,y) for x,y,w in samplings)
                for x, y in set(self.samplings) - detected:
                    samplings.append((x,y,0))

            self.wifis_RBF[wifiID] = Rbf(
                *rotate(samplings),
                function=self.RBF,
                smooth=self.smooth,
                epsilon=self.epsilon
            )

            heatmap = [[
                    self.wifis_RBF[wifiID](x,y)
                    if self.border[y_pos][x_pos]
                    else self.score_outside
                    for x_pos, x in enumerate(self.y_linspace)
                ]   for y_pos, y in enumerate(self.y_linspace)
            ]
            self.wifis_heatmap[wifiID] = heatmap

    def get_position(self, goal, request_strict=False):
        """Return the best matching position according to the request and wifi RBF"""

        #set request
        self.goal = goal
        request = {}
        for wifiID, samplings in self.spots.items():
            result = 0
            for x,y,w in samplings:
                if (x,y) == goal:
                    result = w

            if result > 0 or request_strict:
                request[wifiID] = result

        # remove goal from spots
        old_spots = deepcopy(self.spots)
        self.spots = {
            wifiID:[
                (x,y,w)
                for x,y,w in sampling
                if (x,y) != goal
            ]for wifiID, sampling in self.spots.items()
        }
        self.generate_wifis_heatmap()

        # Position heatmap
        self.heatmap = [[
                self.fct_mean([
                    self.fct_abs(strength, self.wifis_heatmap[wifiID][y][x])
                    for wifiID, strength in request.items()
                ])
                if self.border[y][x]
                else self.score_outside
                for x in range(self.step)
            ]   for y in range(self.step)
        ]

        # Search best position
        w_min, x_min, y_min = float("inf"), None, None
        for y_pos, (y, row) in enumerate(zip(self.y_linspace, self.heatmap)):
            for x_pos, (x, w) in enumerate(zip(self.x_linspace, row)):
                if w < w_min and self.border[y_pos][x_pos]:
                    w_min, x_min, y_min = w, x, y
        self.best = (x_min, y_min)

        self.spots = old_spots
        return self.best

    def benchmark_precision(self):
        """Return the average distance for each spots"""
        distance_sum = 0
        n = len(self.samplings)
        for i, goal in progressbar(
                enumerate(self.samplings),
                len(self.samplings),
                "Benchmark ", 100):

            distance_sum += distance(self.get_position(goal), goal)
        self.precision = distance_sum / n
        return distance_sum / n


def show_heatmap(heatmaps, column=2, cmap_max_hot=True):
    plt.style.use('ggplot')


    if isinstance(heatmaps, Heatmap):
        column = 1
        heatmapInfos = [heatmaps]

    n = len(heatmaps)
    figure, axes = plt.subplots(min(column, n), max(1, math.ceil(n/column)), squeeze=False)
    for i, h in enumerate(heatmaps):
        plot = axes[i%column, i//column]
        plt.xlim(h.x_range)
        plt.ylim(h.y_range)
        #plot.axis('equal')
        if cmap_max_hot:
            cmap = cm.get_cmap('hot')
        else:
            cmap = cm.get_cmap('hot_r')

        cmap.set_under("blue")

        c = plot.pcolormesh(
            h.x_linspace, h.y_linspace,
            h.heatmap, cmap=cmap,
            vmin=h.score_outside+1
        )
        figure.colorbar(c, ax=plot)

        if h.title != None:
            plot.set_title(h.title.capitalize(), loc='left', fontsize=18)

        if h.note != None:
            plot.set_title(h.note, loc='right', fontsize=10)

        plot.scatter(*rotate(h.samplings), c="r", label="samplings")
        if h.goal != None:
            plot.scatter([h.goal[0]], [h.goal[1]], c="g", label="goal")
        if h.best != None:
            plot.scatter([h.best[0]], [h.best[1]], c="c", label="best")

        for (x,y), note in h.annotate.items():
           plot.annotate(note, (x, y + 0.2))

        if False and  h.show_spot_center:
            plot.scatter(
                *rotate([sampling_gravity_center(samplings) for samplings in h.spots.values()]),
                c="y", label="spot center"
            )

        if h.goal != None:
            print(f"Heatmap n°{i} :", h.title,
                  "\nGoal : ", h.goal,
                  "Best : ", h.best,
                  "Distance : ", distance(h.goal, h.best)
            )
        plot.legend()

    for i in range(len(heatmaps), len(axes)*len(axes[0])):
        plot = axes[i%column, i//column]
        plot.axis('off')

    if PGF:
        plt.savefig('heatmap.pgf')
    else:
        plt.show()




if __name__ == "__main__":
    choice = [
        test_localisation,
        test_custom_linear_RBF_on_spots,
        test_scipy_RBFs_on_spots,
    ]

    while True:
        for i, f in enumerate(choice):
            print(f"{i})\t {f.__name__}")
        print()
        c = read("choice : ")
        if c == "":
            c = "0"
        print("")
        choice[int(c)]()
