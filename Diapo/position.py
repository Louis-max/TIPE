self.heatmap = [[
    self.fct_moyenne([
        self.fct_difference(
            qualite,
            self.wifis_heatmap[wifiID][y][x])
        for wifiID, qualite in requete.items()
    ])
    if self.proche[y][x]
    else self.score_exterieur
    for x in range(self.precision)
]   for y in range(self.precision)
]
