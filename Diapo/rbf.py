import numpy as np
import matplotlib.pyplot as plt

xk = np.array([2,4,7,9,12])
x = np.linspace(0, 14, 100)

"""
plt.figure(figsize=(12,6))
plt.plot(xk, true_fn(xk), "x", markersize=15)
plt.plot(x, true_fn(x ), "--r")
plt.show()
"""

"""
2 1
4 3
7 2
9 3
12 4

"""
import math
def euclidean_distance(x, xk):
    return np.sqrt(((x.reshape(-1, 1)) - xk.reshape(1, -1)) **2)
# euclidean_distance(xk, xk)

def kernel(radius, eps):
    return np.exp(-(radius*2/3)**2)
    #return radius**2 * np.log(radius + 0.00001)
# kernel(eculidean_distance(xk, xk), 2)

class RBFInterp(object):
    def __init__(self, eps):
        self.eps = eps
    def fit(self, xk, yk):
        self.xk_ = xk
        transformation = kernel(euclidean_distance(xk, xk), self.eps)
        self.w_ = np.linalg.solve(transformation, yk)
    def __call__(self, xn):
        transformation = kernel(euclidean_distance(xn, xk), self.eps)
        #print("transfo", transformation)
        w = [round(x,2) for x in self.w_]
        print("self.w_", w)
        print("\plotEpsilon{" + "}{".join(list(map(str, w))) + "}{eps};")
        return transformation.dot(self.w_)

interp = RBFInterp(1)
yk = np.array([1,3,2,3,4])
interp.fit(xk, yk)
# interp(x)

plt.figure(figsize=(12,6))
plt.plot(xk, yk, "x")
plt.plot(x, interp(x), "--r")
#plt.plot(x, true_fn(x), "b")
plt.show()
