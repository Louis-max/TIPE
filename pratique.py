"""
Ce programme permet d'enregistrer des points wifis qui sont enregistrées dans le fichier texte wifis.data
"""
import os
import sys
import time
import datetime
from collections import namedtuple
from vracs import *
from théorique import *
from matplotlib import pyplot as plt, cm
from matplotlib import style
#plt.style.use('ggplot')

from PIL import Image

"""
Spots : {spotsID : samplings, ...}
Samplings : [(x,y,weigth), ...]

scans: {scanID:scan, ...}
scan: namedtuple("Scan", "date", "time", "ID", "desc", "x", "y", "wifis")
wifis: [(spotBSSID, spotSSID, rate, signal), ...]

Spots : {wifiID : samplings, ...}
Samplings : [(x,y,weigth), ...]
Request : {spotsID: weigth, ...}
"""
scan = namedtuple("scan", ["date", "time", "ID", "desc", "x", "y", "wifis"])
NUMBER_SCAN_PER_SAMPLING = 20
scans= {}
idd  = 0
HISTORY = ""
BUFFER = []

def read(*args, **kargs):
    """Read one input of the BUFFER"""
    global BUFFER, HISTORY
    if BUFFER == []:
        ans = input(*args, **kargs)
        BUFFER.extend(ans.split(";"))
        HISTORY += ans + ";"
    else:
        print(*args, **kargs, end=f" ({BUFFER[0]})\n")
    return BUFFER.pop(0)

def read_file(path):
    """return the raw content of the file"""
    with open(path, "r") as reader:
        content = reader.read()
        if content == '':
            return "{}" # reading empty file
        return content

def write_file(path, content):
    """writte content to file"""
    with open(path, "w+") as writer:
        writer.writelines(content)


def arrange(txt):
    """Formating file to be saved"""
    for old, new in (
            ("wifis=[", "\n\twifis=[\n\t"), # Beginning of the file
            ("},", "},\n\t"),               # Newline for each wifi
            ("}}),", "}}),\n"),             # Newline for each sampling
    ):
        txt = txt.replace(old, new)
    return txt

def merge():
    """
    Merge scans buffer with the coordinate file.
    Values need to be separeted by a comma
    """
    global scans
    coordinates = read_file("./coordinates.txt").strip().split("\n")
    if len(coordinates) != len(scans.items()):
        debug(f"Coordinate ({len(coordinates)}) and scans ({len(scans.items())}) didn't have the same amount of entry, abord merging.")
        return

    for i, line in enumerate(coordinates):
        x, y = map(float, line.split(","))
        scans[i] = eval(repr(scans[i]).replace(", x=None", f", x={x}").replace(", y=''", f", y={y}"))
    debug("Done !")


def backup_scans(scans):
    """save scans buffer to file in backup directory"""
    write_file("./backup/" + now(), arrange(repr(scans)))

def save_scans():
    """save scans into scans file of the home project"""
    debug("Scans saved !")
    backup_scans(scans)
    write_file("./scans", arrange(repr(scans)))
    write_file("./scans_raw", arrange(repr(scans_raw)))

def load_scans_file():
    """load scans file into buffer"""
    global scans, idd
    scans = eval(read_file("./scans"))
    idd  = max([scan.ID for scan in scans.values()] + [0]) + 1
    debug("File loaded !", len(scans), " loaded, next ID will be", idd)

def clear_scans_file():
    """Backup and empty scans buffer"""
    global scans, idd
    backup_scans(scans)
    debug("Scans has been archived and clear.")
    scans = {}
    idd = 0

def export_heatmap():
    """save heatmap to .dat file"""
    h = heatmaps_buffer[0]

    if False:
        content = ""
        for y, row in enumerate(h.heatmap):
            for x, v in enumerate(row):
                content += f"{x} {y} {round(float(v), 1)}\n"
    else:
        #content = "\n".join([" & ".join([str(int(v)) for v in row]) for row in heatmaps_buffer[0].heatmap])
        cmap = cm.get_cmap("hot")

        img = Image.new('RGB', (h.step, h.step))
        pixels = img.load()
        mini = min(map(min, h.heatmap))
        maxi = max(map(max, h.heatmap))
        def eq(x):
            r = (x-mini)/(maxi-mini)
            return tuple([int(v*255) for v in cmap(r)[:3]])

        for y, row in enumerate(h.heatmap):
            for x, v in enumerate(row):
                pixels[x, y] = eq(v)
        img.save('heatmap.png')
    #write_file("heatmap.dat", content)
    debug("Heatmap exported !")

def run_command():
    """run a command"""
    global idd, scans
    exec(read("Input your command : "))

def get_wifi_infos():
    """return a hasmap {BSSID:{"BSSID":wifiID, "SSID":wifi name, "RATE":int, "SIGNE":int}}"""
    wifis_infos = os.popen("nmcli dev wifi list").read()
    wifis_infos = wifis_infos.split("\n")

    categories = []
    name = ""
    for pos, char in enumerate(wifis_infos[0]):
        if char != " " and name != "" and name[-1] == " ":
            categories.append((name.strip(), pos-len(name), pos))
            name = ""
        name += char
    categories.append((name.strip(), len(wifis_infos[0])-len(name), len(wifis_infos)))

    wifis = {}
    for line in wifis_infos[1:-1:]:
        wifi = {
            key : line[start:end]
            for key, start, end in categories
            if key in ("BSSID", "SSID", "RATE", "SIGNAL")
        }
        wifi["RATE"] = int(wifi["RATE"].replace(" Mbit/s", ""))
        wifi["SIGNAL"] = int(wifi["SIGNAL"])
        wifis[wifi["BSSID"]]=wifi
    return wifis

def delete_sampling():
    """delete one sampling of the scans buffer"""
    global scans, idd
    rm = int(read("ID of the sampling to be deleted : "))
    del scans[rm]

    if idd == rm:
        idd -= 1
        debug("The next ID will be", idd)

scans_raw = {}
def take_sampling(ask=True):
    """append detected wifi to scans buffer"""
    global scans, idd

    wifis = {}
    counter = {}
    for s in progressbar(
            range(NUMBER_SCAN_PER_SAMPLING),
            NUMBER_SCAN_PER_SAMPLING,
            "Scanning ... ", 40):

        for BSSID, infos in get_wifi_infos().items():
            if BSSID not in wifis.keys():
                wifis[BSSID] = infos
                counter[BSSID] = 1

                if not BSSID in scans_raw.keys():
                    scans_raw[BSSID] = {}
            else:
                counter[BSSID] += 1
                scans_raw[BSSID][idd] = infos
                wifis[BSSID]["RATE"] += infos["RATE"]
                wifis[BSSID]["SIGNAL"] += infos["SIGNAL"]
    print()

    for BSSID in wifis.keys():
        wifis[BSSID]["RATE"] = int(wifis[BSSID]["RATE"] / counter[BSSID])
        wifis[BSSID]["SIGNAL"] = int(wifis[BSSID]["SIGNAL"] / counter[BSSID])

    print("Wifis infos :\n", arrange("wifis=[" + repr(wifis) + "]"))


    if len(set(counter.values())) > 1:
        debug(f"\nWarning : All wifi didn't have same frequency : {repr(set(counter.values()))}")
        ans = ""
        while ans not in ("y", "n", "r"):
            ans = "y"
            #ans = read("Keep sampling or retry ? (y/n/r) ")
        if ans == "n":
            return
        if ans == "r":
            take_sampling()
            return


    desc = read(f"\n\tDescription of the sampling of ID {idd} : ") if ask else ""
    debug("Enter coordinate : (blank for None)")
    x = read("\t\tX : ") if ask else ""
    if x == "": x = None
    else : x = float(x)

    y = read("\t\tY : ") if ask else ""
    if y == "": Y = None
    else : y = float(y)
    data = scan(
        now(),
        time.time(),
        idd,
        desc,
        x, y,
        wifis
    )
    scans[idd] = data
    debug(f"Register {idd} created !")
    idd += 1


def leave():
    """save scans buffer and leave"""
    save_scans()
    return "exit"
def leave_force():
    """leave"""
    return "exit"
def clear_history():
    """clear history"""
    global HISTORY
    HISTORY = []
def replay():
    """exec same input as the last time"""
    global BUFFER, HISTORY
    with open("./history", "r") as reader:
        content = reader.read()
        if content == "":
            debug("No history available.")
        else:
            BUFFER = content.strip().split(";")

def save_history():
    """Save history to specific file in saved_history folder"""
    global HISTORY
    name = read("Name of the history conf file : ")
    with open('./saved_history/' + name, 'w+') as writer:
        writer.writelines(repr(HISTORY))
    debug("History saved !")

def load_history():
    """Load and run specifique history file in saved_history"""
    global BUFFER
    # name = read("Name of the history conf file : ")
    name = "debug"
    with open("./saved_history/"+name, "r") as reader:
        BUFFER = reader.read().strip().replace("e", "").replace("L", "").split(";")
        debug("buffer : ",BUFFER)
    debug("History added to buffer !")

def rafale(ask=True):
    """fast wifis detecting tool"""
    try:
        ans = "y"
        while ans != "n":
            take_sampling(ask=False)

            if ask :
                ans = read("Press n to stop, r to redo the last scan, anything else to continue")
            else:
                ans = "y"
                time.sleep(2);
            if ans == "r":
                idd -= 1
                del scans[idd]
    except:
        print("Stop !")

def user_choose(choices, title="Chose one of them :", desc={}):
    """let the user choose a value in choices and return this value"""
    ans = ""
    while ans.isdigit() == False or int(ans) not in range(len(choices)):
        debug(title)
        for n, choice in enumerate(choices):
            desc_message = ""
            if choice in desc.keys():
                desc_message = desc[choice]
            debug(f"\t({n}) : {choice} " + desc_message)
        ans = read(":")
    debug("\n")
    return choices[int(ans)]

heatmaps_buffer = []
def load_heatmap():
    """load one scan file into the heatmap buffer"""
    global heatmaps
    name = user_choose(os.listdir("./saved_scans/"),title="Choose the file to open :")
    scans = eval(read_file("./saved_scans/" + name))
    spots = scans_to_spots(scans)

    RBF = user_choose(
        ["inverse", "gaussian",
         "linear", "cubic",
         "quintic", "thin_plate",
         "multiquadric"], title="Choose one RBF:")
    heatmap = Heatmap(spots, RBF)
    heatmap.title = RBF
    heatmap.annotate = {(scan.x, scan.y): scan.ID for scan in scans.values()}
    heatmaps_buffer.append(heatmap)
    debug(f"{name} loaded !")

def scans_to_spots(scans):
    """convert scans buffer to spots"""
    spots = {}
    for scan in scans.values():
        for infos in scan.wifis.values():
            BSSID = infos["BSSID"]
            if not BSSID in spots.keys():
                spots[BSSID] = []
            spots[BSSID].append((scan.x, scan.y, infos["SIGNAL"]))

    for spot in list(spots.keys()):
        if len(spots[spot]) <= 3:
            del spots[spot]
    return spots

def plot_position_heatmap():
    """Show the position heatmap according to your request"""
    global heatmaps_buffer

    while True:
        spot = int(read("Choose spot : "))
        goal = {
            ID:(x,y) for (x,y),ID in heatmaps_buffer[0].annotate.items()
        }[spot]

        for i in range(len(heatmaps_buffer)):
            heatmaps_buffer[i].get_position(goal)
        show_heatmap(heatmaps_buffer, cmap_max_hot=False)

def open_all_RBF():
    """Open all rbf"""
    global BUFFER
    scan = input("Scan to open : ")
    for idRBF, name in [
            (0,"inverse"),
            (1,"gaussian"),
            (2,"linear"),
            #(3,"cubic"),
            #(4,"quintic"),
            (5,"thin_plate"),
            (6,"multiquadric")
    ]:
        BUFFER.extend(["o", scan, str(idRBF)])

def plot_smooth_test():
    """Benchmark smoothness"""
    global BUFFER
    BUFFER.append("w")
    r = [3, 2, 1, 0]
    for smooth in r:
        BUFFER.extend(["5", str(smooth)])
    print("Buffer : ", BUFFER)

def plot_wifis():
    """plot one specific wifi"""
    while True:
        # extract SSID of wifis
        wifis_name = {}
        for f_name in os.listdir("./saved_scans"):
            scans = eval(read_file("./saved_scans/"+f_name))
            for scanID, scanInfos in scans.items():
                for wifiID, wifiInfos in scanInfos.wifis.items():
                    wifis_name[wifiID] = wifiInfos["SSID"]

        h = heatmaps_buffer[0]
        wifiID = user_choose(sorted(list(h.wifis)), title="Choose one wifiID to plot", desc=wifis_name)
        #epsilon = float(read("Epsilon : "))
        epsilon=None
        #distance = float(read("Border distance : "))

        for i in range(len(heatmaps_buffer)):
            heatmaps_buffer[i].goal = None
            heatmaps_buffer[i].epsilon = epsilon
            #heatmaps_buffer[i].border_distance = distance
            heatmaps_buffer[i].set_border()
            #heatmaps_buffer[i].note = f"epsilon:{round(epsilon, 2)}"

            wifis_spots = deepcopy(heatmaps_buffer[i].spots)
            heatmaps_buffer[i].spots = {wifiID:wifis_spots[wifiID]}
            heatmaps_buffer[i].generate_wifis_heatmap()
            heatmaps_buffer[i].spots = wifis_spots

            heatmaps_buffer[i].heatmap = heatmaps_buffer[i].wifis_heatmap[wifiID]
        show_heatmap(heatmaps_buffer, cmap_max_hot=True)

def benchmark_precision():
    """get the average estimation distance"""
    print("Precision : ", heatmaps_buffer[0].benchmark_precision())

def benchmark_mean_precision():
    """get the average estimation distance depending of the mean function used"""
    means = [mean_arithmetic, mean_geometric, mean_geometric2, mean_harmonic, mean_quadratic]
    result = {}
    for mean in means:
        print(f"Mean : {mean.__name__}")
        heatmaps_buffer[0].fct_mean = mean
        precision = heatmaps_buffer[0].benchmark_precision()
        print("Precision : ", precision)
        result[mean.__name__] = precision
    print("Summary :", result)

import traceback
def main():
    global HISTORY, BUFFER, heatmaps_buffer
    choices = {
        "l":load_scans_file,
        "t":take_sampling,
        "d":delete_sampling,
        "r":rafale,
        "s":save_scans,
        "c":clear_scans_file,

        "e":leave,
        "E":leave_force,

        "R":run_command,

        "o":load_heatmap,
        "p":plot_position_heatmap,
        "w":plot_wifis,
        "D":export_heatmap,

        "!":load_history,
        "replay":replay,
        "clear":clear_history,
        "save hist":save_history,
        "load hist":load_history,

        "P":benchmark_precision,
        "M":benchmark_mean_precision,
        "m":merge,
    }
    while True:
        answer = ""
        result = ""

        try:
            print(f"{len(scans.keys())} scans | {len(heatmaps_buffer)} heatmap loaded | {HISTORY.count(';')} actions")
            for key in "ltdrs opw PMR!e":
                if key == " ":
                    print()
                else:
                    fct = choices[key]
                    print('({}) {:30} : {:40}'.format(key, fct.__name__, fct.__doc__))
            answer = read("Your choice : ")
            if not answer in choices.keys():
                print("\n\n")
                continue

            print("\n"*2)
            result = choices[answer]()
            print("\n"*2)


        #except KeyboardInterrupt:
        #    exit(0)

        except:
            print()
            traceback.print_exc()
            print()
            #read("(paused)\n")
        if answer == "exit" or result == "exit":
            print("History : " + HISTORY)
            with open("history", "w+") as writer:
                writer.writelines(HISTORY)

            exit(0)
if __name__ == "__main__":
    main()
